// Copyright (c) 2020-present,  INSPUR Co, Ltd.  All rights reserved.
// This source code is licensed under Apache 2.0 License.

#include "pure_mem/std_out_logger.h"

#include "rocksdb/options.h"
#include "util/filename.h"
#include "util/sync_point.h"

namespace rocksdb {

Status CreateStdOutLoggerFromOptions(const std::string &dbname,
                                     const DBOptions &options,
                                     std::shared_ptr<Logger> *logger) {
  if (options.info_log) {
    *logger = options.info_log;
    return Status::OK();
  }

  Env *env = options.env;
  std::string db_absolute_path;
  env->CreateDirIfMissing(dbname);

  auto result = new StdOutLogger();
  logger->reset(result);

  return Status::OK();
}
} // namespace rocksdb

