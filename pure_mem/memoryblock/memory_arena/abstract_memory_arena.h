//  Copyright (c) 2020-present,  INSPUR Co, Ltd.  All rights reserved.
// This source code is licensed under Apache 2.0 License.
#pragma once
#include "options/cf_options.h"
#include <pure_mem/key_index/inline_uk_index.h>
#include "rocksdb/slice.h"

namespace rocksdb {

 /**
 * 定义　MemoryArena 的多种不同算法实现
 */
    enum MemIndexType {
        MEM_ARENA = 0,         //
        MEM_ARENA_WITH_ART = 1, //
        MEM_ARENA_WITH_VECTOR = 2,            //
    };

    typedef InlineUserKeyIndex<const MemTableRep::KeyComparator &> ArtTree;

    class IMemoryArena {
    public:
        explicit IMemoryArena(const InternalKeyComparator& mkeyCmp, const ImmutableCFOptions& ioptions) {};

        virtual ~IMemoryArena() {};

        virtual bool Insert(const Slice& key, const Slice& value) = 0;

        virtual bool RangeDeletionInsert(const Slice& key, const Slice& value) = 0;

        virtual ArtTree::Iterator *Seek(const Slice& key) = 0;


        virtual size_t GetTotalMemVolume() const = 0;

    };
}