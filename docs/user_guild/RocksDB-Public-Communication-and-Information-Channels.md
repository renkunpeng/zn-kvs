# RocksDB Public Communication Channels

## RocksDB Official Website
https://rocksdb.org/

## Slack
rocksdb.slack.com
(You can apply or ask someone to invite to join)

## Google Group
https://groups.google.com/forum/#!forum/rocksdb

## Facebook Public Group (RocksDB Developers Public)
https://www.facebook.com/groups/rocksdb.dev/

## Twitter
https://twitter.com/RocksDB

## Meetup General Information
https://www.meetup.com/RocksDB

