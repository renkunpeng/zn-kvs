# CockRoachDB Benchmark Test

## Introduction

CockRoachDB use Rocksdb as Storage engine.  CockRoachDB has many storage engine  benchmark tests. We use them as outer benchmark tools. 

## Latency of common operation

### Insert Latency (us)

![Insert](images/op_put_latency.png)

### Get Latency (us)

![Get](images/op_get_latency.png)

### Scan Latency (us)

![Scan](images/op_scan_latency.png)